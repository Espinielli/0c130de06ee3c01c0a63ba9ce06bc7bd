A reconstruction of one of the cubic globes as [described by Carlos Furuti](http://www.progonos.com/furuti/MapProj/Normal/ProjPoly/projPoly2.html).

See also [Earth in a Cube I](http://bl.ocks.org/espinielli/1018c88657010f8ee93ea4224652e3cf).

This is D3v3 with the addition of a `polyhedron.js` from
[Jason Davies](https://www.jasondavies.com/maps/) that I originally used in
[Cahill-Keyes](http://bl.ocks.org/espinielli/b6e2f37814a19dc461f8)' map projection.
This library unfortunatelly never made it officially into D3 geo...

Forked from <a href='http://bl.ocks.org/espinielli/'>espinielli</a>'s block: <a href='http://bl.ocks.org/espinielli/b6e2f37814a19dc461f8'>Cahill-Keyes blues</a>

forked from <a href='http://bl.ocks.org/espinielli/'>espinielli</a>'s block: <a href='http://bl.ocks.org/espinielli/1018c88657010f8ee93ea4224652e3cf'>Earth in a Cube</a>